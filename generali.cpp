#include <iostream>
#include <string>
#include <fstream>
#include <sstream>
#include <cmath>
#include <assert.h>
#include <queue>


using std::cin;
using std::cout;
using std::endl;
using std::string;
using std::ofstream;
using std::ifstream;
using std::ostringstream;
using std::pow;
using std::queue;

class General;
//#generals
int N;
//#battles
int M;
//
bool exist_cycle;


bool found_cycle;
int not_generals_index;
int cycle;
int n_counselors;
int n_U;
int n_V;
General **V;
General **U;
int *Dist;
General **Pair;/// should be a member of variable;
General **counselors;
General *vDummy;
//General *uDummy;// maybe useless

class Node{
public:
	General *actual;
	Node *next;
	Node *previous;
};
void Third_Law_Remover();
void removeBattleInCycle(Node *node);
bool BFS_HK();
void Hopcrof_Karp();
class General{
public:
	static int n_gens; //index in the generals vector
	static int n_visit;
	int n_defeated; //number of generals defeated by this general
	int n_defeated_by; //number of generals that defeated this general
	//here be a linked list of generals defeated
	Node battles;
	//this should be a pointer to the last element added, making "add new" an O(1) operation
	Node *last_added;
	int begin_visit = 0;
	int end_visit = 0;
	int name;
	int cycle_id;
	int u_time;

	General(){
		//this sets the name of the general as its index in the array

		cycle_id = -1;
		name = n_gens;
		n_gens++;
		u_time = -1;
		n_defeated = 0;
		n_defeated_by = 0;
		battles.actual = this;
		battles.next =	&battles;
		battles.previous = &battles;
		//technically, this is the last battle added
		last_added = &battles;
		//WARNING//problems with huge amount of data;//WARNING!!!!
	};
	void addDefeatedGeneral(General &g);
	void addDefeatedGeneralOnTrasp(General &g, Node *previous);
	void addDefeatedGeneralForDummy(General &g);
	void printDefeatedGenerals();
	void removeGeneral(Node *node, int next);
	void DFS(int *order_of_not_generals);
	void RemoverDFS(General *generals);
	void FinalDFS(ostringstream &str);
	bool DFS_HK();
	void resetBattles();
};


//initializing the number of generals to 0
int General::n_gens = 0;
int General::n_visit = 0;
void General::resetBattles(){
	Node *newBattles = new Node();
	battles = *newBattles;
	n_defeated = 0;
	n_defeated_by = 0;
	battles.actual = this;
	battles.next =	&battles;
	battles.previous = &battles;
		//technically, this is the last battle added
	last_added = &battles;
}
void General::DFS(int *order_of_not_generals){
	if(begin_visit == 0){
		Node *temp;
		temp = battles.next;
		begin_visit = ++n_visit;
		for(int i = 0;i < n_defeated;i++){
			//visits only if it hasn't been visited before
			if((temp->actual->begin_visit) == 0){
				temp->actual->DFS(order_of_not_generals);
			}else
			exist_cycle = true;
			
			temp = temp->next;
		}
		end_visit = ++n_visit;
		
		//change order of generals on array according to end_visit (bigger end_visit in front of smaller end_visti)
		
		
		order_of_not_generals[not_generals_index] = this->name;
		
		--not_generals_index;

	}
}

void General::RemoverDFS(General *generals){
	if(begin_visit == 0){
		found_cycle = true;
		//cout << this->name << " Hello " << this->cycle_id << endl;
		this->cycle_id = cycle;
		//cout << this->name << " Hello " << this->cycle_id << endl;
		generals[this->name].cycle_id = cycle;
		Node *temp;
		temp = battles.next;
		begin_visit = ++n_visit;
		this->u_time = cycle;
		V[n_V++] = &(generals[this->name]);
		vDummy->addDefeatedGeneralForDummy(generals[this->name]);

		for(int i = 0;i < n_defeated;i++){
			//cout << "Going in1" << endl;
			//visits only if it hasn't been visited before
			if((temp->actual->begin_visit) == 0){
				//cout << name << " calls DFS on " << temp->actual->name << endl;
				temp->actual->RemoverDFS(generals);
			}
			if(temp->actual->end_visit == 0 || (temp->actual->end_visit && temp->actual->cycle_id == this->cycle_id )){// if white or Gray
				//cout << temp->actual->name <<" Removed N: " << this->name << endl;
				
				removeBattleInCycle(temp->previous);
				
				generals[temp->actual->name].n_defeated--;
			}

			if(temp->actual->end_visit && temp->actual->cycle_id != this->cycle_id && cycle != temp->actual->u_time ){
				temp->actual->u_time = cycle;
				U[n_U++] = &(generals[temp->actual->name]);
			}
			
			temp = temp->next;

		}
		end_visit = ++n_visit;
	}
}

void General::FinalDFS(ostringstream &str){
	if(begin_visit == 0){
		Node *temp;
		temp = battles.next;
		begin_visit = ++n_visit;
		for(int i = 0; i < n_defeated;i++){
			//visits only if it hasn't been visited before
			if((temp->actual->begin_visit) == 0){
				//assert(this->cycle_id != temp->actual->cycle_id);
			//	cout << name << " calls DFS on " << temp->actual->name << endl;
			//	cout << "HI DUDE" << str.str() << endl;
				str << this->name << " " << temp->actual->name << endl;//WARNING//problems with huge amount of data;//WARNING!!!!
			//	cout << "HI DUDE :D" << str.str() << endl;
				temp->actual->FinalDFS(str);
			}
			
			temp = temp->next;
		}
		end_visit = ++n_visit;
		
		//change order of generals on array according to end_visit (bigger end_visit in front of smaller end_visti)

	}
}

void General::addDefeatedGeneralForDummy(General &g){
	//if n_defeated = 0 
	Node *node = new Node();
	node->actual = &g;
	//node->previous = last_added->next
//
//	
	node->next = last_added->next;//to first
	node->previous = last_added;//to current last but to change
	(last_added->next)->previous = node;
	last_added->next = node;
	
	//node->next = &battles;
	last_added = node;
	n_defeated++;

}
void General::addDefeatedGeneral(General &g){
	//if n_defeated = 0 
	Node *node = new Node();
	node->actual = &g;

	node->next = last_added->next;//to first
	node->previous = last_added;//to current last but to change
	(last_added->next)->previous = node;
	last_added->next = node;
	
	last_added = node;

	n_defeated++;
	g.n_defeated_by++;
}

void General::addDefeatedGeneralOnTrasp(General &g, Node *previous){
	//if n_defeated = 0 
	Node *node = new Node();
	node->actual = &g;

	last_added->next = node;
	node->previous = previous;
	node->next = &battles;
	last_added = last_added->next;
	n_defeated++;
	g.n_defeated_by++;
}


void General::removeGeneral(Node *node, int next){
	//cout <<  " REMOV :D" << next << endl;
	/*cout << " DIMINUSHED IN " << node->next->actual->name << endl;
	cout << " previuos " << node->actual->name << endl;
	cout << " to remove " << node->next->actual->name << endl;*/
	///////
	if((--node->next->actual->n_defeated_by) == 0)
		counselors[n_counselors++] = node->next->actual;

	node->next = node->next->next;
	--this->n_defeated;

	///
}

void General::printDefeatedGenerals(){
	Node *temp;
	temp = battles.next;
	cout << " out " << n_defeated << ", in" << n_defeated_by << " :";
	cout << name << " defeated :";
	for(int i = 0;i < n_defeated;i++){
		//cout << name << " defeated " << temp->actual->name << endl;
		cout << " " << temp->actual->name;
		temp = temp->next;
	}
	cout << endl;
}

bool BFS_HK(){
	queue<General*> q;
	General *u;
	for(int i = 0; i < n_U; i++){
		if(Pair[U[i]->name] == vDummy){
			Dist[U[i]->name] = 0;
			q.push(U[i]);
			//cout << "Hello BFS dist 0" << endl;
		}else{
			Dist[U[i]->name] = 2*N;
			//cout << "Hello BFS dist N+1" << endl;
		}
	}
	Dist[vDummy->name] = 2*N;
	Node *v;
	while(q.empty() == false){
    //	cout << "Hello BFS inside while" << endl;
		u = q.front();
		q.pop();
		if (Dist[u->name] < Dist[vDummy->name]){
      //  	cout << "Hello BFS inside while if got it" << endl;
			v = &(u->battles);
			v = v->next;
			for(int i = 0; i < u->n_defeated;i++){
				if(v->actual->cycle_id == cycle){
				//	cout << "IN BFS from " << u->name <<" on " << v->actual->name << endl;
					if( Dist[Pair[v->actual->name]->name] == 2*N){
				//		cout << "IN IF BFS from " << u->name <<" on " << v->actual->name << endl;
						Dist[ Pair[v->actual->name]->name] = Dist[u->name] + 1;
				//		cout << "IN IF BFS from " << u->name << " DIST of paired " << v->actual->name << " " << Pair[v->actual->name]->name << endl; 
						q.push(Pair[v->actual->name]);
					}
				}
				v = v->next;
			}
		}
	}
	return Dist[vDummy->name] != 2*N;
}

bool General::DFS_HK(){
	if (this->name != vDummy->name){
		Node *v;
		v = &(this->battles);
		for(int i = 0; i < n_defeated;i++){
			v = v->next;
			//this->printDefeatedGenerals();
			//cout << "cycle -> " << cycle << "v->cycle id of" <<v->actual->name << " is "<< v->actual->cycle_id << endl;
			if(v->actual->cycle_id == cycle){
				//cout << "cycle if" << endl;
				if (Dist[Pair[v->actual->name]->name] == Dist[this->name] + 1){
					//cout << "dist if" << endl;
					if ((Pair[v->actual->name])->DFS_HK() == true){
						Pair[v->actual->name] = this;
						Pair[this->name] = v->actual;
					//	cout << "IN DFS PAIRED u " << this->name << " with " << Pair[this->name]->name << endl;
					//	cout << "IN DFS PAIRED v " << v->actual->name << " with " << Pair[v->actual->name]->name << endl;
					//	int i;
					//	cin >> i;
						return true;

					}
				}

			
			}
		}
	Dist[this->name] = 2*N;
				return false;
	}
	return true;
}

void Hopcrof_Karp(){
	for(int i = 0; i < n_U; i++){
		Pair[U[i]->name] = vDummy;
	}
	for(int i = 0; i < n_V; i++){
		Pair[V[i]->name] = vDummy;
	}
	//cout << "Hello DUMMY paired whit itself " << Pair[vDummy->name] << " " << vDummy << endl;
		//Dist[vDummy->name] = 2*N;
	while(BFS_HK()){
		//cout << "vDummy "<< U[n_U-1]->name << " vDummy Dist " << Dist[U[n_U-1]->name] << endl;
		//cout << "Printing Pairs "<< n_U << " " << n_V << endl;
	/*	for(int i = 0; i < n_U; i++){
			cout << U[i]->name << " -> " << Pair[U[i]->name]->name << endl;
			cout << U[i]->name << " dist " << Dist[U[i]->name] << endl;
				//	U[i]->printDefeatedGenerals();
		}*/
	//	int k;
	//	cin >> k;
	//	cout << " calling DFS "<< endl;
		for(int i = 0; i < n_U; i++){
			if(Pair[U[i]->name] == vDummy){
				U[i]->DFS_HK();
			}
		}
		//break;
	}

		//cout << "To be removed HAPPiLY" << endl;
	Third_Law_Remover();
		//cout << "HELLO SHOULD HAVE removed HAPPiLY" << endl;

}

void Third_Law_Remover(){
	General *pair;
	Node *temp;
	int limit;
		for(int i = 0; i < n_U-1; i++){//
				pair = Pair[U[i]->name];
				temp = &(U[i]->battles);
				limit = U[i]->n_defeated;
				for(int j = 0; j < limit; j++){
					temp = temp->next;
					if(temp->actual->name != pair->name && temp->actual->cycle_id == cycle){
					//	cout << "in REmover"<< U[i]->name << " -> " << Pair[U[i]->name]->name << endl;
					//	cout << U[i]->name <<" REMOVED " << temp->actual->name << endl;
						removeBattleInCycle(temp);
						(U[i]->n_defeated)--;
					}
					
				}
		}

	}

	void removeBattleInCycle(Node *node){
		/*Node *temp;
		temp = &(node->head->battles);
		for(int i = 0;i < node->head->n_defeated;i++){
		//cout << name << " defeated " << temp->actual->name << endl;
			cout << " " << temp->actual->name;
			temp = temp->next;
		}
		cout << endl;
		Node *previous_temp = node->previous;
		cout << node->id << " Actually removed " << node->actual->name << endl;
		cout << "previous and next and next next " << node->previous->actual->name << " " << node->actual->name << " " << node->next->actual->name  <<endl;
*/
		if(--node->actual->n_defeated_by == 0)
			counselors[n_counselors++] = node->actual;
		node->next->previous = node->previous;
		(node->previous)->next = node->next;

/*
		cout << "previous and next " << previous_temp->actual->name << " " << previous_temp->next->actual->name << endl;
		temp = &(node->head->battles);
		for(int i = 0;i < node->head->n_defeated;i++){
		//cout << name << " defeated " << temp->actual->name << endl;
			cout << " " << temp->actual->name;
			temp = temp->next;
		}
		cout << endl;*/
	}



	int main(){
	//input
		ifstream input("input.txt");
		input >> N;
		input >> M;


	//for each general we index who he was beaten by
		General generals[N];

	//need a way to reset counter for the generals here
		General::n_gens = 0;
		General not_generals[N];
		int puppa[N];
		int *order_of_not_generals = puppa;
		General *auxCounselors[N];
		counselors = auxCounselors;
		General *auxPair[N+1];
		Pair = auxPair;
		int auxDist[N+1];
		Dist = auxDist;
		General *auxV[N];
		V = auxV;
		General *auxU[N];
		U = auxU;
		General auxVDummy;
		vDummy = &auxVDummy;
		vDummy->name = N;
		n_V = 0;
		n_U = 0;

		ostringstream tree;
		not_generals_index = N-1;
		int winner;
		int loser;
		for(int i = 0;i < M;i++){
			input >> winner;
			input >> loser;
			generals[winner].addDefeatedGeneral(generals[loser]);
			not_generals[loser].addDefeatedGeneralOnTrasp(not_generals[winner],generals[winner].last_added);
		}
	//cout << "Cycles REMOVED >:|" << endl;
	/*	for(int i = 0;i < N;i++){
			generals[i].printDefeatedGenerals();
		//not_generals[i].DFS();
		}*/

			for(int i = 0;i < N;i++){
		//cout << "General #" << generals[i].name << endl;
				if(generals[i].n_defeated_by == 0){
					counselors[n_counselors++] = &generals[i];
				}
				generals[i].DFS(order_of_not_generals);
		//not_generals[i].DFS();
			} 

	/*for(int i = 0;i < N;i++){
		cout << "General #" << generals[i].name << " time_visited[" << generals[i].begin_visit << ", " << generals[i].end_visit << "]" << endl;
	}*/


/*	for(int i = 0;i < N;i++){
		cout << "General #" << not_generals[order_of_not_generals[i]].name << endl;//<< " time_visited[" << generals[i].begin_visit << ", " << generals[i].end_visit << "]" << endl;
	}*/

	//generals[0].removeGeneral(generals[0].battles.next->next,2);

		found_cycle = false;
		if(exist_cycle){
			for(int i = 0;i < N;i++){
			//cout << "General #" << not_generals[order_of_not_generals[i]].name << endl;
		//generals[i].DFS();
				n_V = 0;
				n_U = 0;
				cycle = i;
				not_generals[order_of_not_generals[i]].RemoverDFS(generals);
				if(n_U != 0 && n_V > 1 && found_cycle){
					U[n_U++] = vDummy;
					/*int c;
					cin >> c;*/
					Hopcrof_Karp();

					vDummy->resetBattles();
				}
				found_cycle = false;
				///
			} 
		}


		for(int i = 0;i < N;i++){
			//generals[i].printDefeatedGenerals();
		//not_generals[i].DFS();
		} 
		for(int i = 0;i < N;i++){
			generals[i].end_visit  = 0;
			generals[i].begin_visit = 0;
		//not_generals[i].DFS();
		} 
		ostringstream str;
		str << n_counselors << endl;
		for(int i = 0;i < n_counselors;i++){
		//cout << "General #" << not_generals[order_of_not_generals[i]].name << endl;
		//generals[i].DFS();
			str << (counselors[i])->name << " ";
			counselors[i]->FinalDFS(tree);//WARNING//problems with huge amount of data;//WARNING!!!!);
} 
	//WARNING//problems with huge amount of data;//WARNING!!!!
	/*for(int i = 0; i < N; i++){

		if((generals[i].str.tellp() != 0)){
			++n_counselors;
			str << i << " ";
			ss << generals[i].str.str();
		}else if((generals[i].n_defeated == 0 && generals[i].n_defeated_by == 0)){ 
				++n_counselors;
				str << i << " ";
		}

	}*/

	ofstream output("output.txt");
	output << str.str() << endl << tree.str();


		/*for(int i = 0;i < N;i++){
			cout << "General #" << generals[i].name << endl;
				//gen//WARNING//problems with huge amount of data;//WARNING!!!!erals[i].DFS();
		//not_generals[i].DFS();
		} */

			return 0;
		}

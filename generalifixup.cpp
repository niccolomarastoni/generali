#include <iostream>
#include <string>
#include <fstream>
#include <sstream>
#include <algorithm>

using std::cin;
using std::cout;
using std::endl;
using std::string;
using std::ofstream;
using std::ifstream;
using std::ostringstream;

class General;
//#generals
int N;
//#battles
int M;
//
bool exist_cycle;

int not_generals_index;
int cycle;
int n_counselors;
General **counselors;

/* WORK IN PROGRESS:
1) Transform classes in structs 
[USELESS]: http://stackoverflow.com/questions/92859/what-are-the-differences-between-struct-and-class-in-c

2) Minimize for loops (and unroll loops)
[IN PROGRESS]

3) Use inline functions when possible
[NONE]

4) Minimize function calls (move loops inside functions, not the inverse)
[NONE]

5) Optimize if..else chains (switch works better for multiple jumps || put most common clause at beginning)
[NONE]

6) Optimize order of array indices (array[i][j] and array[i][j+1] are adjacent to each other
									array[i][j] and array[i+1][j] may be arbitrarily far apart)
[NONE]

7) Minimize number of local variables and function parameters
[NONE]

8) Switch to initialization instead of assignment
[NONE]

9) Use constructor initializer lists. (Use Color::Color() : r(0), g(0), b(0) {} rather than Color::Color() { r = g = b = 0; } .)
[WAT]

10) Use shift operations >> and << instead of integer multiplication and division, where possible.
[NONE]

11) Minimize use of the heap
[NONE]



/* PERFORMANCE:
- Original code:
- Optimized code:

*/

class Node{
public:
	General *actual;
	Node *next;
	Node *previous;
};

void removeBattleInCycle(Node *node);
class General{
public:
	static int n_gens; //index in the generals vector
	static int n_visit;
	int n_defeated; //number of generals defeated by this general
	int n_defeated_by; //number of generals that defeated this general
	//here be a linked list of generals defeated
	Node battles;
	//this should be a pointer to the last element added, making "add new" an O(1) operation
	Node *last_added;
	int begin_visit = 0;
	int end_visit = 0;
	int name;
	int cycle_id;

	General(){
		//this sets the name of the general as its index in the array
		cycle_id = -1;
		name = n_gens;
		n_gens++;

		n_defeated = 0;
		n_defeated_by = 0;
		battles.actual = this;
		//technically, this is the last battle added
		last_added = &battles;
		//WARNING//problems with huge amount of data;//WARNING!!!!
	};
	void addDefeatedGeneral(General &g);
	void addDefeatedGeneralOnTrasp(General &g, Node *previous);
	void printDefeatedGenerals();
	void removeGeneral(Node *node, int next);
	void DFS(int *order_of_not_generals);
	void RemoverDFS(General *generals);
	void FinalDFS(ostringstream &str);

	
	
};


//initializing the number of generals to 0
int General::n_gens = 0;
int General::n_visit = 0;

void General::DFS(int *order_of_not_generals){
	if(begin_visit == 0){
		Node *temp;
		temp = battles.next;
		begin_visit = ++n_visit;
		for(int i = 0;i < n_defeated;i++){
			//visits only if it hasn't been visited before
			if((temp->actual->begin_visit) == 0){
				cout << name << " calls DFS on " << temp->actual->name << endl;
				temp->actual->DFS(order_of_not_generals);
			}else
			exist_cycle = true;
			
			temp = temp->next;
		}
		end_visit = ++n_visit;
		
		//change order of generals on array according to end_visit (bigger end_visit in front of smaller end_visti)
		
		
		order_of_not_generals[not_generals_index] = this->name;
		
		--not_generals_index;

	}
}

void General::RemoverDFS(General *generals){
	if(begin_visit == 0){
		this->cycle_id = cycle;
		Node *temp;
		temp = battles.next;
		begin_visit = ++n_visit;
		for(int i = 0;i < n_defeated;i++){
			//cout << "Going in1" << endl;
			if(temp->actual->end_visit == 0 ||(temp->actual->end_visit && temp->actual->cycle_id == this->cycle_id )){// if white or Gray
				
				removeBattleInCycle(temp->previous);
				generals[temp->actual->name].n_defeated--;
			}
			//visits only if it hasn't been visited before
			if((temp->actual->begin_visit) == 0){
				//cout << name << " calls DFS on " << temp->actual->name << endl;
				temp->actual->RemoverDFS(generals);
			}
			
			temp = temp->next;

		}
		end_visit = ++n_visit;
	}
}

void General::FinalDFS(ostringstream &str){
	if(begin_visit == 0){
		Node *temp;
		temp = battles.next;
		begin_visit = ++n_visit;
		for(int i = 0; i < n_defeated;i++){
			//visits only if it hasn't been visited before
			if((temp->actual->begin_visit) == 0){
			//	cout << name << " calls DFS on " << temp->actual->name << endl;
			//	cout << "HI DUDE" << str.str() << endl;
				str << this->name << " " << temp->actual->name << endl;//WARNING//problems with huge amount of data;//WARNING!!!!
			//	cout << "HI DUDE :D" << str.str() << endl;
				temp->actual->FinalDFS(str);
			}
			
			temp = temp->next;
		}
		end_visit = ++n_visit;
		
		//change order of generals on array according to end_visit (bigger end_visit in front of smaller end_visti)

	}
}

void General::addDefeatedGeneral(General &g){
	//if n_defeated = 0 
	Node *node = new Node();
	node->actual = &g;

	last_added->next = node;
	node->previous = last_added;
	node->next = &battles;
	last_added = last_added->next;
	n_defeated++;
	g.n_defeated_by++;
}
void General::addDefeatedGeneralOnTrasp(General &g, Node *previous){
	//if n_defeated = 0 
	Node *node = new Node();
	node->actual = &g;

	last_added->next = node;
	node->previous = previous;
	node->next = &battles;
	last_added = last_added->next;
	n_defeated++;
	g.n_defeated_by++;
}

void General::removeGeneral(Node *node, int next){
	//cout <<  " REMOV :D" << next << endl;
	/*cout << " DIMINUSHED IN " << node->next->actual->name << endl;
	cout << " previuos " << node->actual->name << endl;
	cout << " to remove " << node->next->actual->name << endl;*/
	///////
	if((--node->next->actual->n_defeated_by) == 0)
		counselors[n_counselors++] = node->next->actual;

	node->next = node->next->next;
	--this->n_defeated;

	///
}

void General::printDefeatedGenerals(){
	Node *temp;
	temp = battles.next;
	cout << " out " << n_defeated << ", in" << n_defeated_by << " :";
	cout << name << " defeated :";
	for(int i = 0;i < n_defeated;i++){
		//cout << name << " defeated " << temp->actual->name << endl;
		cout << " " << temp->actual->name;
		temp = temp->next;
	}
	cout << endl;
}

void removeBattleInCycle(Node *node){
	if(--node->actual->n_defeated_by == 0)
		counselors[n_counselors++] = node->actual;
	(node->previous)->next = node->next;
}

int main(){

	//input
	ifstream input("inputfixup.txt");
	input >> N;
	input >> M;


	//for each general we index who he was beaten by
	General generals[N];

	//need a way to reset counter for the generals here
	General::n_gens = 0;
	General not_generals[N];
	int *order_of_not_generals = new int[N];
	counselors = new General*[N];
	ostringstream tree;
	not_generals_index = N-1;
	int winner;
	int loser;
	for(int i = 0;i < M;i++){
		input >> winner;
		input >> loser;
		generals[winner].addDefeatedGeneral(generals[loser]);
		not_generals[loser].addDefeatedGeneralOnTrasp(not_generals[winner],generals[winner].last_added);
	}


	for(int i = 0;i < N;i++){
		//cout << "General #" << generals[i].name << endl;
		if(generals[i].n_defeated_by == 0){
			counselors[n_counselors++] = &generals[i];
		}
		generals[i].DFS(order_of_not_generals);
		//not_generals[i].DFS();
	} 

	for(int i = 0;i < N;i++){
		cout << "General #" << generals[i].name << " time_visited[" << generals[i].begin_visit << ", " << generals[i].end_visit << "]" << endl;
	}

	cout << "After First DFS" << endl;
	
/*	for(int i = 0;i < N;i++){
		cout << "General #" << not_generals[order_of_not_generals[i]].name << endl;//<< " time_visited[" << generals[i].begin_visit << ", " << generals[i].end_visit << "]" << endl;
	}*/

	//generals[0].removeGeneral(generals[0].battles.next->next,2);

		for(int i = 0;i < N;i++){
			generals[i].printDefeatedGenerals();
		//not_generals[i].DFS();
		} 

		if(exist_cycle){
			cout << " There is a at least on cylce :D" << endl;
			for(int i = 0;i < N;i++){
			//cout << "General #" << not_generals[order_of_not_generals[i]].name << endl;
		//generals[i].DFS();
				cycle = i;
				not_generals[order_of_not_generals[i]].RemoverDFS(generals);
			} 
		}



		cout << "Cycles REMOVED >:|" << endl;
		for(int i = 0;i < N;i++){
			generals[i].printDefeatedGenerals();
		//not_generals[i].DFS();
		} 
		for(int i = 0;i < N;i++){
			generals[i].end_visit  = 0;
			generals[i].begin_visit = 0;
		//not_generals[i].DFS();
		} 
		ostringstream str;
		str << n_counselors << endl;
		for(int i = 0;i < n_counselors;i++){
		//cout << "General #" << not_generals[order_of_not_generals[i]].name << endl;
		//generals[i].DFS();
			str << (counselors[i])->name << " ";
		counselors[i]->FinalDFS(tree);//WARNING//problems with huge amount of data;//WARNING!!!!);
} 
	//WARNING//problems with huge amount of data;//WARNING!!!!
	/*for(int i = 0; i < N; i++){

		if((generals[i].str.tellp() != 0)){
			++n_counselors;
			str << i << " ";
			ss << generals[i].str.str();
		}else if((generals[i].n_defeated == 0 && generals[i].n_defeated_by == 0)){ 
				++n_counselors;
				str << i << " ";
		}

	}*/

		cout << str.str() << " coselor " << endl << tree.str();


		/*for(int i = 0;i < N;i++){
			cout << "General #" << generals[i].name << endl;
				//gen//WARNING//problems with huge amount of data;//WARNING!!!!erals[i].DFS();
		//not_generals[i].DFS();
		} */

			return 0;
		}